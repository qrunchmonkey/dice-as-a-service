# Dice, As A Service

`yarn build-server` or `yarn watch-server`

## Dice Server
The service is currently running at http://dice-as-a-service.moon.engineering/dice/roll

Free to use, but please be nice to my server.

## Example Usage in Streamlabs Chatbot
`$dummy $username: $readapi(http://localhost:3000/dice/roll.txt?q=$msg)`

## Endpoints:

`/dice/roll?q={query}`
`/dice/roll.txt?q={query}`

## Parser Notes:

Supported opperators: +, -, /, \*, ^, ()
Standard PEDMAS order of operations.

Plus, you can use dice: `d6`, `d20`, etc.

Or multiple dice: `4d6`, `2d8`, `64d2`, etc.

Dice can have a number of sides defined by an expression: `d(2^4)`, `d(d20)`

Or a custom list of sides: `d[2,4,6,8,10,12]` - roll a 6 sided die with even numbered sides

And of course the number of dice you roll can also be an expression: `(2^4)d20`, `(d6)d20`

And, if you're a monster, you can combine all of this to make some kind of franken-meta-die:
`((d6)d(d20))d(64 ^ d2)`

In english...Roll d6 many dice each with d20 number of sides. Then roll that many dice, each with with 64 to the power of a coin flip sides. (and take the sum of those dice.)

## Todo:
- Better error handling, always return an error document.
- Better Endpoints
- Opt in to getting parser errors
- More formatting options for .txt endpoint
- Rate Limiting?
- BigNum support?
