import * as Koa from 'koa';
import * as helmet from 'koa-helmet';
import * as winston from 'winston';

import { config } from './config';
import { logger } from './logging';

import { router } from './routes';


const app = new Koa();

app.use(helmet());
app.use(logger(winston as any));

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        ctx.body = err.message;
        ctx.app.emit('error', err, ctx);
    }
});


app.use(router.routes());


app.listen(config.port);
console.log(`Server running on port ${config.port}`);
