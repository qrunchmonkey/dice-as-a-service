# Based on https://github.com/Hardmath123/nearley/tree/master/examples/calculator
# And inspired by https://github.com/justjake/dicetower

@builtin "number.ne"
@builtin "whitespace.ne"
@builtin "postprocessors.ne"

@{%
    var dice = require('./dice_util');

    function wrapIfNeeded(data) {
        if (typeof data === 'number' || (Array.isArray(data) && data.length === 1)) {
            return data;
        } else{
            return [data];
        }
    }

%}

expression -> _ AS _ {% nth(1) %}

# PEMDAS!
# We define each level of precedence as a nonterminal.

# Parentheses
PP  -> "(" _ AS _ ")" {% function(d) {return wrapIfNeeded(d[2]); } %}
    | N             {% id %}
P   -> PP            {% id %}
    | DICE            {% id %}



# Exponents
E -> P _ "^" _ E    {% function(d) {return ['pow', d[0], d[4]]; } %}
    | P             {% id %}

# Multiplication and division
MD -> MD _ "*" _ E  {% function(d) {return ['mul', d[0], d[4]]; } %}
    | MD _ "/" _ E  {% function(d) {return ['div', d[0], d[4]]; } %}
    | E             {% id %}

# Addition and subtraction
AS -> AS _ "+" _ MD {% function(d) {return ['add', d[0], d[4]]; } %}
    | AS _ "-" _ MD {% function(d) {return ['sub', d[0], d[4]]; } %}
    | MD            {% id %}

# A number or a function of a number
N -> decimal          {% id %}
    | "pi"            {% () => Math.PI %}
    | "e"             {% () => Math.E %}
    | "phi"             {% () => 1.6180339887498948482 %}


DICE -> PP "d" PP                     {% function(d){return new dice.DiceResult({count: d[0], sides: d[2]}); } %}
    | "d" PP                         {% function(d){return new dice.DiceResult({count: 1, sides: d[1]}); } %} # implicitly bag of one die.
    | "d" "[" DICE_SIDES_LIST "]" {% function(d){return new dice.DiceResult({count: 1, arrayOfSides: d[2]})} %} # implicitly bag of one die.
    | PP "d" "[" DICE_SIDES_LIST "]" {% function(d){return new dice.DiceResult({count: d[0], arrayOfSides: d[3]})} %}

DICE_SIDES_LIST -> _ P _            {% function(d){return [d[1]]; } %}
    | DICE_SIDES_LIST _ "," _ P _   {% function(d){return d[0].concat(d[4]); } %}
