import * as Random from 'random-js';
import * as Winston from 'winston';

const mt = Random.engines.mt19937().autoSeed();

export function calculate(tree: any): number {
  if (Array.isArray(tree)) {
    if (tree.length === 3) {
      const [op, rA, rB] = tree;
      const a = calculate(rA);
      const b = calculate(rB);
      switch (op) {
        case 'add':
          return a + b;
        case 'sub':
          return a - b;
        case 'mul':
          return a * b;
        case 'div':
          return a / b;
        case 'pow':
          return Math.pow(a, b);
        default:
          throw new Error('Unimplimented Op');
      }
    } else if (tree.length === 1) {
      return calculate(tree[0]);
    } else {
      throw new Error('Weird array length: ' + tree.length + 'for tree: ' + JSON.stringify(tree));
    }
  } else if (typeof tree === 'number') {
    return tree;
  } else if (tree instanceof DiceResult) {
    return tree.finalValue;
  } else {
    // dice?
    throw new DiceError(DiceErrorReason.Unknown, 'Unimplimented Value' + tree);
  }
}

export function format(tree: any, depth = 0): string {
  if (typeof tree === 'number') {
    return tree.toString();
  } else if (Array.isArray(tree)) {
    if (tree.length === 1) {
      if (depth) {
        return '(' + format(tree[0], depth + 1) + ')';
      } else {
        return format(tree[0], depth + 1);
      }
    } else if (tree.length === 3) {
      const [op, rA, rB] = tree;
      const a = format(rA, depth + 1);
      const b = format(rB, depth + 1);
      switch (op) {
        case 'add':
          return a + ' + ' + b;
        case 'sub':
          return a + ' - ' + b;
        case 'mul':
          return a + ' * ' + b;
        case 'div':
          return a + ' / ' + b;
        case 'pow':
          return a + '^' + b;
        default:
          throw new Error('Unimplimented Op');
      }
    } else {
      throw new DiceError(
        DiceErrorReason.Unknown, 'Weird array length on format: ' + tree.length + 'for tree: ' + JSON.stringify(tree));
    }
  } else if (tree instanceof DiceResult) {
    const d = tree;
    let dieFormat: string;
    if (d.sides) {
      // dice has custom sides...
      // dieFormat = `${d.count}d[${d.sides.join(", ")}]`
      const formattedSides = d.rawSides.map((v) => format(v, 2));
      dieFormat = `${d.count}d[${formattedSides.join(', ')}]`;
    } else {
      dieFormat = `${d.count}d${d.numberOfSides}`;
    }
    if (depth > 1) {
      if (d.finalResults && d.finalResults.length) {
        return `${dieFormat}(=${d.finalResults.join(', ')})`;
      } else {
        return `${dieFormat}(=${d.finalValue})`;
      }
    } else {
      return dieFormat;
    }
  } else {
    // dice?
    throw new Error('Unimplimented Value');
  }
}

export interface ICritResult {
  totalDice: number;
  eligibleDice: number;
  failures: number;
  successes: number;
}
export function crits(tree: any): ICritResult {

function applyDie(c: ICritResult, d: DiceResult) {
  const result = c;
  result.eligibleDice += d.critEligible;
  result.failures += d.critFailures;
  result.successes += d.critSucesses;
  result.totalDice += d.count;
  return result;
}

function reducer(result: ICritResult, current: any): ICritResult {
  if (Array.isArray(current)) {
    return current.reduce(reducer, result);
  } else if (current instanceof DiceResult) {
    return applyDie(result, current);
  } else {
    return result;
  }
}

return reducer({
  eligibleDice: 0,
  failures: 0,
  successes: 0,
  totalDice: 0
}, tree);
}

// Dice
export const MAX_DICE_COUNT = 1 << 24; // (= 2^24 = 16777216)
export const MAX_SAVED_DICE_RESULTS = 11;
export const MIN_SIDES_FOR_CRIT = 10;
export class DiceResult {
  public numberOfSides: number;
  public sides?: number[];
  public rawSides: any[];
  public count: number;
  public finalValue: number;
  public finalResults: number[];
  public critSucesses: number = 0;
  public critFailures: number = 0;
  public critEligible: number = 0;
  constructor(options: {
    count: number | any[];
    sides?: number | any[];
    arrayOfSides?: any[];
  }) {
    Winston.log('info', 'setting up die');
    const { count, sides, arrayOfSides } = options;
    // validate...
    let numberOfDice: number;
    if (typeof count === 'number') {
      numberOfDice = count;
    } else {
      numberOfDice = calculate(count);
    }

    if (numberOfDice < 1) {
      throw new DiceError(DiceErrorReason.LessThanOneDie);
    } else if (!Number.isInteger(numberOfDice)) {
      throw new DiceError(DiceErrorReason.FractionalDice, numberOfDice.toLocaleString());
    } else if (numberOfDice > MAX_DICE_COUNT) {
      throw new DiceError(DiceErrorReason.TooManyDice);
    }
    numberOfDice = Math.round(numberOfDice);
    let numberOfSides: number;

    const results: number[] = [];
    let sum: number = 0;

    if (arrayOfSides) {
      // something...
      console.log(arrayOfSides);
      const sidesArray = arrayOfSides.map((side) => {
        if (typeof side === 'number') {
          return side;
        } else if (side instanceof DiceResult) {
          return side.finalValue;
        } else if (Array.isArray(side)) {
          const value = calculate(side);
          if (value === undefined) {
            throw new DiceError(
              DiceErrorReason.InvalidListOfSides,
              format(side),
            );
          }
          return value;
        } else {
          throw new DiceError(DiceErrorReason.InvalidListOfSides, '' + side);
        }
      });

      numberOfSides = sidesArray.length;
      this.sides = sidesArray;
      this.rawSides = arrayOfSides;

      // roll with custom sides array
      for (let i = 0; i < numberOfDice; i++) {
        const r = Random.pick(mt, sidesArray);
        sum += r;
        if (i <= MAX_SAVED_DICE_RESULTS) {
          // push one too many results, then none get forwarded.
          results.push(r);
        }
      }
    } else {
      if (typeof sides === 'number') {
        if (sides < 1) {
          throw new DiceError(DiceErrorReason.LessThanOneSide);
        } else if (!Number.isInteger(sides)) {
          throw new DiceError(DiceErrorReason.FractionalSides, sides.toLocaleString());
        }
        numberOfSides = sides;
      } else {
        numberOfSides = calculate(sides);
        if (numberOfSides < 1) {
          throw new DiceError(DiceErrorReason.LessThanOneSide, format(sides));
        }
      }
      numberOfSides = Math.round(numberOfSides);

      // roll dice with standard sides
      for (let i = 0; i < numberOfDice; i++) {
        const r = Random.integer(1, numberOfSides)(mt);
        if (numberOfSides >= MIN_SIDES_FOR_CRIT) { // this die roll counts for crit stats.
          this.critEligible += 1;
          if (r === 1) {
            this.critFailures += 1;
          } else if (r === numberOfSides) {
            this.critSucesses += 1;
          }
        }
        sum += r;
        if (i <= MAX_SAVED_DICE_RESULTS) {
          // push one too many results, then none get forwarded.
          results.push(r);
        }
      }
    }

    // now all values are valid.
    this.numberOfSides = numberOfSides;
    this.count = numberOfDice;
    this.finalValue = results.reduce((a, b) => a + b, 0);
    if (results.length < MAX_SAVED_DICE_RESULTS) {
      this.finalResults = results;
    }
  }
}

// Errors:
import * as util from 'util';
export enum DiceErrorReason {
  Unknown,
  LessThanOneSide,
  LessThanOneDie,
  InvalidListOfSides,
  TooManyDice,
  FractionalDice,
  FractionalSides,
}
export class DiceError {
  public name: string;
  public reason: DiceErrorReason;
  public inExpression?: string;

  constructor(reason: DiceErrorReason, inExpression?: string) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.reason = reason;
    this.inExpression = inExpression;
  }
}
util.inherits(DiceError, Error);
