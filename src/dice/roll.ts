import * as Nearley from 'nearley';
import * as diceutil from './dice_util';
const grammar = require('./dice_grammar.ne.js');

export interface IRollResult {
  result?: string;
  formatted?: string;
  sum?: number;
  error?: string;
  tree?: any,
  crits?: any,
}

export function parse(s: string): IRollResult {
  let ans;
  const expression = s.trim();
  if (expression === '') { throw new diceutil.DiceError(diceutil.DiceErrorReason.Unknown, 'Empty Expression'); }

  try {

    ans = new Nearley.Parser(grammar.ParserRules, grammar.ParserStart).feed(expression);

    if (ans.results.length) {
      // console.log("result: ", ans.results);
      const formatted = diceutil.format(ans.results);
      const sum = diceutil.calculate(ans.results);
      const crits = diceutil.crits(ans.results);
      return {
        formatted, sum,
        result: `${formatted} = ${sum}`,
        tree: ans.results,
        crits
      };
    } else {
      return {
        error: `That doesn't look like a complete expression. Check your parenthesis.`,
      };
    }
  } catch (e) {
    if (e instanceof RangeError) {
      return {
        error: `I can't roll dice with more than nine quadrillion sides. (2^53)`,
      };
    } else if (e instanceof diceutil.DiceError) {
      let reason: string = `Something went wrong while rolling ${expression}`;
      switch (e.reason) {
        case diceutil.DiceErrorReason.LessThanOneDie:
          reason = 'I can\'t roll less than one die.';
          break;
        case diceutil.DiceErrorReason.LessThanOneSide:
          reason = 'I can\'t roll dice that have less than one side.';
          break;
        case diceutil.DiceErrorReason.InvalidListOfSides:
          reason = `I can't figure out how to roll a die with "${
            e.inExpression
          }" on a side.`;
          break;
        case diceutil.DiceErrorReason.TooManyDice:
          reason = 'I\'m not allowed to roll more than 16,777,216 dice at once.';
          break;
        case diceutil.DiceErrorReason.FractionalDice: 
          reason = `I can't roll ${e.inExpression} dice.`;
          break;
        case diceutil.DiceErrorReason.FractionalSides:
          reason = `I can't figure out how to roll a die with ${e.inExpression} sides.`
        default:
          throw e;
      }
      return {
        error: reason,
      };
    } else {
      if (typeof e.offset === 'number') {
        const unparsed = expression.substring(e.offset);
        const lastToken = unparsed.split(/\s+/)[0];
        return {
          error: `I don't understand what you meant by "${lastToken}"`,
        };
      } else {
        throw e;
      }
    }
  }
}
