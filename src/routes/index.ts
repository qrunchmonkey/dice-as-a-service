import * as Router from 'koa-router';
import {router as diceRouter} from './dice';

export const router = new Router();
router.use('/dice', diceRouter.routes(), diceRouter.allowedMethods());
