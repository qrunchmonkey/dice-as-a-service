import * as Router from 'koa-router';
import * as Roll from '../dice/roll';

const router = new Router();

/*
regex that tests if the string is a "simple" expression with one diebag with less than 99 dice with less than 99 sides.
matches things like:
  - d6
  - 10d4
  - 2 * d10
  - 44d2 + 50
*/
const exp_is_simple_diebag = /^(?:\d+\s*[+\-*\/]\s*)?\d{0,2}d\d{1,2}(?:\s*[+\-*\/]\s*\d+)?$/;
const exp_contains_metadie = /(?:\)d|\)d\(|d\()/
router.get('/', async (ctx) => {
  ctx.body = { response: 'ok' };
});

router.get('/roll.txt', async (ctx) => {
  const q = (ctx.query.q || '').trim();
  const newline = ctx.query.seperator || '\n';

  const roll = Roll.parse(q);
  let response = '';
  if (roll.error) {
    let query = q;
    if (query === '') {
      query = '...absolutely nothing';
      response = `Tried to roll: ${query}`;
    } else {
      response = `Tried to roll: ${query}${newline}But, ${roll.error}`;
    }
  } else { //Success
    if (!q.includes('d')) {
      response = `Computed ${roll.formatted} and got ${roll.sum.toLocaleString()}`;
    } else if (exp_is_simple_diebag.test(q)) {
      if (roll.crits.successes && roll.crits.successes > roll.crits.eligibleDice * 0.20) {
        response = `Rolled ${q} and got${newline}CurseLit CurseLit CurseLit ${roll.crits.successes} Crits! CurseLit CurseLit CurseLit${newline}...and total of ${roll.sum.toLocaleString()}`;
      } else {
        response = `Rolled ${q} and got ${roll.sum.toLocaleString()}`;
      }

    } else if (Math.abs(q.length - roll.formatted.length) > 6 || exp_contains_metadie.test(q)) {
      // if the difference between the query length and the formatted tree is substantial...
      response = `Rolling ${q}...${newline}Rolled: ${roll.formatted} and got ${roll.sum.toLocaleString()}`;
    } else {
      response = `Rolled ${roll.formatted} and got ${roll.sum.toLocaleString()}`;
    }
  }
  ctx.body = response;
});

router.get('/roll', async (ctx) => {
  ctx.body = Roll.parse(ctx.query.q || '');
});
export { router };
